class Fixed
  include ActionView::Helpers::SanitizeHelper

  require 'open-uri'
  require 'nokogiri'

  BASE_URL = 'https://developer.github.com/v3/'

  def initialize

    html = open(BASE_URL)
    doc  = Nokogiri.HTML(html)

    doc.css('script').remove
    doc.css('style').remove

    doc.xpath("//@*[starts-with(name(),'on')]").remove
    content_string = strip_tags doc.to_s
    @status_count  = content_string.upcase.scan(/STATUS/).count

  end

  def calculate_margin
    @status_count
  end
end
