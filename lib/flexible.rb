class Flexible
  include ActionView::Helpers::SanitizeHelper

  require 'open-uri'
  require 'nokogiri'


  BASE_URL = 'http://www.reuters.com/'

  def initialize

    html = open(BASE_URL)
    doc  = Nokogiri.HTML(html)

    doc.css('script').remove
    doc.css('style').remove

    doc.xpath("//@*[starts-with(name(),'on')]").remove
    content_string = strip_tags doc.to_s
    @a_count        = content_string.upcase.scan(/A/).count

  end

  def calculate_margin
    @a_count == 0 ? @a_count : (@a_count.to_f/100.00)
  end
end
