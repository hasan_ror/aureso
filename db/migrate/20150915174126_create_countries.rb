class CreateCountries < ActiveRecord::Migration
  def change
    create_table :countries do |t|
      t.string :name
      t.integer :country_code
      t.integer :organization_id

      t.timestamps
    end
  end
end
