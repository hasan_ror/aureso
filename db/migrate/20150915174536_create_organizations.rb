class CreateOrganizations < ActiveRecord::Migration
  def change
    create_table :organizations do |t|
      t.integer :country_id
      t.integer :parent_organization_id
      t.string :name
      t.string :public_name
      t.string :organization_type
      t.string :pricing_policy

      t.timestamps
    end
  end
end
