Rails.application.routes.draw do

  devise_for :admin_users, {class_name: 'User'}.merge(ActiveAdmin::Devise.config)
  ActiveAdmin.routes(self)

  get 'dashboards/index'

  root to: "dashboards#index"

  post 'model_type_prices/:organization_id', to: 'model_type_prices#create'

end
