ActiveAdmin.register Country do
  permit_params :id, :name, :country_code, :organization_id, organizations_attributes: [:id, :name, :public_name, :organization_type, :pricing_policy]

  form do |f|
    f.inputs do
      f.input :name
      f.input :country_code
    end

    f.inputs "Organizations" do
      unless f.object.organizations.present?
        (5).times do
          f.object.organizations.build
        end
      end
      f.fields_for :organizations do |organization|
        organization.inputs do
          organization.input :name
          organization.input :public_name
          organization.input :organization_type
          organization.input :pricing_policy, as: :select, collection: PRICING_POLICY

        end
      end
    end
    f.actions
  end

  show do
    attributes_table do
      row :name
      panel "Organizations" do
        table_for country.organizations do
          column "name" do |organization|
            link_to organization.name, admin_countries_path
          end
        end
      end
    end
  end

  controller do

    def create
      if Country.all.count >= 3
        flash[:alert] = "You cannot create more than 3 countries!"
      else
        Country.create(name: params[:country][:name], country_code: params[:country][:country_code], organization_id: params[:country][:organization_id])
        flash[:notice] = "Country Created Successfully"
      end
      redirect_to admin_countries_path
    end
  end
end