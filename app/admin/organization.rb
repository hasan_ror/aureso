ActiveAdmin.register Organization do
  permit_params :id, :country_id, :parent_organization_id, :name, :public_name, :organization_type, :pricing_policy,
                child_organizations_attributes: [:id, :country_id, :parent_organization_id, :name, :public_name, :organization_type, :pricing_policy],
                locations_attributes: [:id, :name, :address, :organization_id]

  form do |f|
    f.inputs do
      f.input :country
      # f.input :country_id, as: :select, collection: Country.all, :member_label => lambda { |i| "#{i.name} - #{i.country_code}" }
      f.input :name
      f.input :public_name
      f.input :organization_type
      organization.input :pricing_policy, as: :select, collection: PRICING_POLICY
      f.input :parent_organization_id, as: :select, collection: Organization.all, :member_label => lambda { |i| "#{i.name} - #{i.public_name}" }
    end

    f.inputs "Locations" do
      (2 - (f.object.locations.count)).times do
        f.object.locations.build
      end
      f.fields_for :locations do |organization|
        organization.inputs do
          organization.input :name
          organization.input :address
        end
      end
    end
    f.actions
  end

  show do
    attributes_table do
      row :name
      panel "Parent Organization" do
        if organization.parent_organization
          table_for organization.parent_organization do
            column "Parent Organization Name" do |p_org|
              link_to p_org.name, admin_organization_path(p_org.id)
            end
          end
        end
      end
    end
  end

  # controller do
  #
  #   def create
  #     if Country.all.count >= 3
  #       flash[:alert] = "You cannot create more than 3 countries!"
  #     else
  #       Country.create(name: params[:country][:name], country_code: params[:country][:country_code], organization_id: params[:country][:organization_id])
  #       flash[:notice] = "Country Created Successfully"
  #     end
  #     redirect_to admin_countries_path
  #   end
  # end
end