class DashboardsController < ApplicationController

  def index
    if current_admin_user.blank?
      redirect_to new_admin_user_session_path
    elsif current_admin_user.admin
      redirect_to admin_dashboard_path
    end
  end

end
