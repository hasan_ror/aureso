class ModelTypePricesController < ApplicationController

  skip_before_filter :verify_authenticity_token, :only => [:create]

  respond_to :json

  def create
    @organization = Organization.find params[:organization_id]
    margin       = Object.const_get(@organization.pricing_policy).new.calculate_margin
    @price       = params[:base_price].to_f + margin
  end

  respond_to do |format|
    format.json {render :json => {name: @organization.name, price: @price}.to_json}
    format.html {render :json => {name: @organization.name, price: @price}.to_json}
  end
end
