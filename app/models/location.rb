# == Schema Information
#
# Table name: locations
#
#  id              :integer          not null, primary key
#  name            :string(255)
#  address         :text
#  organization_id :integer
#  created_at      :datetime
#  updated_at      :datetime
#

class Location < ActiveRecord::Base

  belongs_to :organization
end
