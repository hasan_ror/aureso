# == Schema Information
#
# Table name: countries
#
#  id              :integer          not null, primary key
#  name            :string(255)
#  country_code    :integer
#  organization_id :integer
#  created_at      :datetime
#  updated_at      :datetime
#

class Country < ActiveRecord::Base

  has_many :organizations

  accepts_nested_attributes_for :organizations, :reject_if => lambda { |a| a[:name].blank? }
end
