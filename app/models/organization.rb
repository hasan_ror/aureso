# == Schema Information
#
# Table name: organizations
#
#  id                     :integer          not null, primary key
#  country_id             :integer
#  parent_organization_id :integer
#  name                   :string(255)
#  public_name            :string(255)
#  type                   :string(255)
#  pricing_policy         :string(255)
#  created_at             :datetime
#  updated_at             :datetime
#

class Organization < ActiveRecord::Base

  # self.inheritance_column = nil

  belongs_to :country
  belongs_to :parent_organization, :class_name => "Organization"

  has_many :child_organizations, :class_name => "Organization", :foreign_key => "parent_organization_id"

  has_many :locations

  accepts_nested_attributes_for :locations, :reject_if => lambda { |a| a[:name].blank? }

end
